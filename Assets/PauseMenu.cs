﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
	GameObject[] pauseObjects;

	// Use this for initialization
	void Start() {
		Time.timeScale = 1;
		pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause");
		hidePaused();
	}

	// Update is called once per frame
	void Update() {

		//uses the p button to pause and unpause the game
		if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Space)  || Input.GetKeyDown(KeyCode.Escape)  || Input.GetKeyDown(KeyCode.Return)  || Input.GetKeyDown(KeyCode.KeypadEnter)) {
			if (Time.timeScale == 1) {
				Time.timeScale = 0;
				showPaused();
			}
			else if (Time.timeScale == 0) {
				Debug.Log("high");
				Time.timeScale = 1;
				hidePaused();
			}
		}

		Debug.LogWarning(Time.timeScale);
	}


	//Reloads the Level
	public void Reload() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	//controls the pausing of the scene
	public void pauseControl() {
		if (Time.timeScale == 1) {
			Time.timeScale = 0;
			showPaused();
		}
		else if (Time.timeScale == 0) {
			Time.timeScale = 1;
			hidePaused();
		}
	}

	//shows objects with ShowOnPause tag
	public void showPaused() {
		foreach (GameObject g in pauseObjects) {
			g.SetActive(true);
		}
	}

	//hides objects with ShowOnPause tag
	public void hidePaused() {
		foreach (GameObject g in pauseObjects) {
			Time.timeScale = 1;
			g.SetActive(false);
		}
	}

	//loads inputted level
	public void Exit() {
	
		Application.Quit(0);
	}
}


public static class ScriptableUtility {
	
	public static (Sprite, AudioClip) Get(ScrapSettings.ScrapType scrapType, List<EnumToSprite.SingleScrap>singleScraps)
	{
		EnumToSprite.SingleScrap singleScrap = singleScraps.Find(s => s.type == scrapType);
		return (singleScrap.broken, singleScrap.audioClip);
	}
    
	public static (Sprite, int) Get(ScrapSettings.ScrapType scrapType1,ScrapSettings.ScrapType scrapType2, List<EnumToSprite.DoubleScrap> doubleScraps  ) {
        
		EnumToSprite.DoubleScrap doubleScrap = doubleScraps.Find(s => s.type1 == scrapType1 && s.type2 == scrapType2  || s.type2 == scrapType1 && s.type1 == scrapType2);
		return (( doubleScrap.repaired, doubleScrap.score) );
	}
}