﻿using System.Collections;
using System.Collections.Generic;
using DesignPatterns;
using UnityEngine;

public class Scrap : StateMachine<Scrap>
{
	private Rigidbody2D rb;
	private int mask;
	private Vector3 mousePosition;
	private Camera cam;
	public ScrapSettings.ScrapType type;
		
	public float minHeight = -10f;

	
	protected override void Awake()
	{
		base.Awake();
		cam = Camera.main;
		this.State=new Falling(this,Vector2.zero);	
		mask = LayerMask.GetMask("Scrap");
		rb = GetComponent<Rigidbody2D>();
		
	}
	

	public void Destroy() {
		gameObject.SetActive(false);
	}

	private void OnDisable() {
        this.State = new Falling(this, Vector2.zero);
		CancelInvoke();
	}

	protected override void Update()
	{
		base.Update();
		if (transform.position.y < minHeight) {
			FindObjectOfType<LevelManager>().IncreaseErrors();
			Invoke(nameof(Destroy),0);
		}
		
	}

	public void AddForce(Vector2 direction, float force)
	{
		rb.AddForce(force*direction);
	}

	public void ChangeSprite(Sprite sprite)
	{
		GetComponent<SpriteRenderer>().sprite = sprite;
	}
	
	public bool CheckClick()
	{
		
		RaycastHit2D hit = Physics2D.GetRayIntersection(cam.ScreenPointToRay(Input.mousePosition), mask);
		if (hit.collider!=null && hit.collider.gameObject == gameObject) {
			return true;
		}
		return false;

	}
	
}