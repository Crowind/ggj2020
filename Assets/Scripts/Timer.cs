﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
	public float maxTime = 30;
	private float currentTime = 30;
	public Text timerText;

	private void Start()
	{
		currentTime = maxTime;
	}

	private void Update()
	{
		currentTime -= Time.deltaTime;
		if (currentTime <= 0)
		{
			Debug.Log("timerFinito, imposta la prossima scena");
			DontDestroyOnLoad(LevelManager.instance);
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

		}
		Debug.Log(currentTime);
		if (currentTime >= 0)
		{
			timerText.text = currentTime.ToString("F0");
		}
		else
		{
			timerText.text = "0";
		}
	}
}
