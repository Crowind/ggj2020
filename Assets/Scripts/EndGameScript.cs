﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndGameScript : MonoBehaviour
{
    public int score;
    public Text endScore; 

    public void Awake() {
        score = LevelManager.instance.Score;
        endScore.text = "Total Score: " + score.ToString();
        Destroy(LevelManager.instance);
    }


    public void LevelScene() {
        SceneManager.LoadScene("Gameplay");
    }

    public void QuitGame() {
        Application.Quit();
    }


}
