﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.Video;
using Random = System.Random;

public class SoundManager : MonoBehaviour
{
    private AudioSource[] audioSources;
    public AudioClip[] LaunchSound;
    public AudioClip[] ScoreSound;


    private void Start()
    {
        audioSources = GetComponents<AudioSource>();
        
    }

    public void PlaySound(AudioClip audioClip, AudioClip audioClip2)
    {
        
        audioSources[0].clip = audioClip;
        audioSources[0].Play();

        audioSources[1].clip = audioClip2;
        audioSources[1].Play();
    }
    
    public void PlaySound(AudioClip audioClip)
    {
        audioSources[0].clip = audioClip;
        audioSources[0].Play();
    }

    public void PlaySound(int score) {

        AudioSource audioSource = gameObject.AddComponent<AudioSource>();

        
        if (score >14)
        {
            audioSource.clip = ScoreSound[2];
            audioSource.Play();
        }
        else if (score>9)
        {
            audioSource.clip = ScoreSound[1];
            audioSource.Play();

        }
        else
        {
            audioSource.clip = ScoreSound[0];
            audioSource.Play();

        }
        
        Destroy(audioSource,audioSource.clip.length+0.1f);
    }

    public void PlayLaunchSound()
    {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();

        var randomNumber = UnityEngine.Random.Range(0, LaunchSound.Length);
        audioSource.clip = LaunchSound[randomNumber];
        audioSource.Play();
        Destroy(audioSource, audioSource.clip.length + 0.1f);    
    }
    
}
