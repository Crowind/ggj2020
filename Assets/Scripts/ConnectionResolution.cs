﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class ConnectionResolution : MonoBehaviour {

	[Range(0.1f, 8f)] public float resolutionTime = 2;
	public Material lowScore;
	public Material mediumScore;
	public Material highScore;
	private LineRenderer lineRenderer;
	public AnimationCurve easingCurve;

	public Sprite scrap1;
	public Sprite scrap2;

	public ScrapSettings.ScrapType scrapType1;
	public ScrapSettings.ScrapType scrapType2;

	public Vector3 scrapPos1;
	public Vector3 scrapPos2;

	private GameObject spriteObj1;
	private GameObject spriteObj2;

	private Vector3 startPoint;
	private Vector3 endPoint;

	private Material curMat;
	
	public Text scoreText;

	public void Init() {
		lineRenderer = GetComponent<LineRenderer>();
		StartCoroutine(Resolution());

		startPoint = lineRenderer.GetPosition(0);
		endPoint = lineRenderer.GetPosition(lineRenderer.positionCount - 1);

		spriteObj1 = new GameObject("Sprite 1");
		spriteObj2 = new GameObject("Sprite 2");


		spriteObj1.transform.position = scrapPos1;
		spriteObj2.transform.position = scrapPos2;

		Transform myTransform = transform;
		spriteObj1.transform.parent = myTransform;
		spriteObj2.transform.parent = myTransform;

		var spriteRenderer1 = spriteObj1.AddComponent<SpriteRenderer>();
		var spriteRenderer2 = spriteObj2.AddComponent<SpriteRenderer>();


		spriteRenderer1.sprite = scrap1;
		spriteRenderer2.sprite = scrap2;
		
		EnumToSprite[] enumToSprite = Resources.LoadAll<EnumToSprite>("");
		
		int score =  ScriptableUtility.Get(scrapType1,scrapType2, enumToSprite[0].doubleScraps).Item2;

		curMat = score <= 9 ? lowScore : score <= 14 ? mediumScore : highScore;

		lineRenderer.material = curMat;
		spriteRenderer1.material = curMat;
		spriteRenderer2.material = curMat;
		scoreText.gameObject.SetActive(false);


	}

	private IEnumerator Resolution() {

		float startTime = Time.time;

		Color startColor = lineRenderer.startColor;
		Color endColor = startColor;
		endColor.a = 0;

		//Vector3 midPoint = (startPoint + endPoint) / 2;
		var startPoints = new Vector3[lineRenderer.positionCount];
		lineRenderer.GetPositions(startPoints);
		while (Time.time < startTime + resolutionTime) {

			var tmpPoints = new Vector3[lineRenderer.positionCount];
			lineRenderer.GetPositions(tmpPoints);
			for (int i = 1; i < tmpPoints.Length - 1; i++) {
				float alpha = easingCurve.Evaluate(((Time.time - startTime) / resolutionTime));
				Vector3 finalPoint = Vector3.Lerp(startPoint, endPoint, t: (float)i / tmpPoints.Length);
				tmpPoints[i] = Vector3.LerpUnclamped(startPoints[i], finalPoint, alpha);
			}
			lineRenderer.SetPositions(tmpPoints);

			lineRenderer.material.color = Color.Lerp(startColor, endColor, (Time.time - startTime) / resolutionTime);
			yield return null;
		}
		Destroy(spriteObj1);
		Destroy(spriteObj2);

		startTime = Time.time;

		Destroy(lineRenderer);
		var newObj = new GameObject();
		newObj.transform.parent = transform;
		newObj.transform.position = (startPoint + endPoint) / 2;
		SpriteRenderer renderer = newObj.AddComponent<SpriteRenderer>();
		
		
		EnumToSprite[] enumToSprite = Resources.LoadAll<EnumToSprite>("");

		(Sprite sprite, int score) = ScriptableUtility.Get(scrapType1,scrapType2, enumToSprite[0].doubleScraps);


		FindObjectOfType<ScrapSpawnerPool>().IncreaseDifficulty();
		FindObjectOfType<SoundManager>().PlaySound(score);


		scoreText.gameObject.SetActive(true);
		scoreText.text = "+" + score+"!";
		scoreText.color = curMat.GetColor("_Highlight");

		renderer.sprite = sprite;
		LevelManager.instance.Score += score;
		

		while (Time.time < startTime + resolutionTime) {

			yield return null;
		}

		Destroy(gameObject);
		startTime = Time.time;
		
		while (Time.time < startTime + resolutionTime) {

			yield return null;
		}
		scoreText.gameObject.SetActive(false);
	}
}