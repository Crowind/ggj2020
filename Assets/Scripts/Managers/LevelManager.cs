﻿using System.Collections;
using System.Collections.Generic;
using DesignPatterns;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : Singleton<LevelManager> {
	public int health = 3;
	private int score;

	public enum Difficulty {
		EASY,
		NORMAL,
		HARD
	};

	public Text tmpScore;

	protected override void Awake() {
		base.Awake();
	}

	public int Score {
		get => score;
		set {
			score = value;
			tmpScore.text = ("Score:") + score;
		}
	}

	//Difficulty d = Difficulty.EASY;
	void Update() {

		if (Input.GetKeyDown(KeyCode.S)) {
			Score += 10;
			tmpScore.text = ("Score:") + Score;
		}
	}

	public void IncreaseErrors() {
		health--;
		
	}

}