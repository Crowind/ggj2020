﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    bool isPaused = false;

    public void Update() {
        if (isPaused) {
            isPaused = OnPause();
        }
    }

    public void LevelScene() {
        SceneManager.LoadScene(1);
    }

    public void ReloadScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitGame() {
        Application.Quit();
    }

    public bool OnPause() {
        if(Time.timeScale == 0) {
            Time.timeScale = 1f;
            return false;
        }
        else {
            Time.timeScale = 0f;
            return true;
        }
    }
}
