﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DesignPatterns;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(LineRenderer))]
public class ConnectManager : Singleton<ConnectManager> {

	[Range(0, 10)] public int smoothedPoints = 5;
	[Range(0.001f, 1)] public float minUnitDistance;

	public GameObject connectionsRendering;

	public Material noSelection;
	public Material selection;
	public Text deltaScore;
	
	private LineRenderer lineRenderer;
	private Queue<Scrap> scrapsQueue;
	private Vector3 lastMousePos;
	private Camera cam;
	private readonly int capacity = 2;

	protected override void Awake() {
		base.Awake();
		scrapsQueue = new Queue<Scrap>();
		cam = Camera.main;
		lineRenderer = GetComponent<LineRenderer>();
	}

	private void Update() {
		if (Time.timeScale > 0) {
			DrawLine();
		}
	}

	private void DrawLine() {
		
		if (Input.GetMouseButton(0) && scrapsQueue.Count == 0) {
			
			lineRenderer.sharedMaterial = noSelection;
			LineGrow();
			if (lineRenderer.positionCount > 20) {
				var lastPositions = new Vector3[21];
				lineRenderer.GetPositions(lastPositions);
				lastPositions = lastPositions.Skip(1).ToArray();
				lineRenderer.positionCount = 20;
				lineRenderer.SetPositions(lastPositions);
			}
		}
		if (Input.GetMouseButton(0) && scrapsQueue.Count>0) {

			if (lineRenderer.sharedMaterial == noSelection) {
				Vector3 lastPos = lineRenderer.GetPosition(lineRenderer.positionCount - 1);
				lineRenderer.positionCount = 1;
				lineRenderer.SetPosition(0,lastPos);
			}
			lineRenderer.material = selection;
			LineGrow();
		}
		if (scrapsQueue.Count > 1) {
			Scrap scrap1 = scrapsQueue.Dequeue();
			Scrap scrap2 = scrapsQueue.Dequeue();

			Connect(scrap1, scrap2);
			lineRenderer.positionCount = 0;
		}
		if(!Input.GetMouseButton(0)) {
			ClearQueue();
		}
	}

	private void Connect(Scrap a, Scrap b) {
		GameObject obj = Instantiate(connectionsRendering, transform);

		var resolution = obj.GetComponent<ConnectionResolution>();
		var connection = obj.AddComponent<LineRenderer>();
		var tmpPositions = new Vector3[lineRenderer.positionCount];
		lineRenderer.GetPositions(tmpPositions);
		connection.startColor = lineRenderer.startColor;
		connection.widthCurve = lineRenderer.widthCurve;
		connection.positionCount = lineRenderer.positionCount;
		connection.SetPositions(tmpPositions);
		connection.SetPosition(0,a.transform.position);
		connection.SetPosition(lineRenderer.positionCount-1,b.transform.position);
	
		EnumToSprite[] enumToSprite = Resources.LoadAll<EnumToSprite>("");
                       
		
		resolution.scrap1 =  ScriptableUtility.Get(a.type, enumToSprite[0].singleScraps).Item1;
		resolution.scrap2 =  ScriptableUtility.Get(b.type, enumToSprite[0].singleScraps).Item1;

		resolution.scrapType1 = a.type;
		resolution.scrapType2 = b.type;

		
		resolution.scrapPos1 = a.transform.position;
		resolution.scrapPos2 = b.transform.position;

		resolution.scoreText = deltaScore;
		
		resolution.Init();

		a.gameObject.SetActive(false);
		b.gameObject.SetActive(false);
		
		
		
	}

	public void ClearQueue() {
		lineRenderer.positionCount = 0;
		scrapsQueue.Clear();
	}

	public void Add(Scrap scrap) {
		scrapsQueue.Enqueue(scrap);
		if (scrapsQueue.Count > capacity) {
			scrapsQueue.Dequeue();
		} 
	}

	private void LineGrow() {
		if (Vector3.Distance(Input.mousePosition, lastMousePos) > minUnitDistance) {

			lineRenderer.SetPosition(lineRenderer.positionCount++, cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)));

		}
		lastMousePos = Input.mousePosition;

		if (smoothedPoints > 0) {

			if (lineRenderer.positionCount > smoothedPoints) {
				Vector3[] tmpPositions = new Vector3[smoothedPoints];

				for (int i = smoothedPoints - 1; i >= 0; i--) {
					tmpPositions[i] = lineRenderer.GetPosition(lineRenderer.positionCount - smoothedPoints - 1 + i);
				}
				tmpPositions = Curver.MakeSmoothCurve(tmpPositions, 1);

				lineRenderer.positionCount = lineRenderer.positionCount - smoothedPoints + tmpPositions.Length;

				for (int i = tmpPositions.Length - 1; i >= 0; i--) {
					lineRenderer.SetPosition(lineRenderer.positionCount - tmpPositions.Length - 1 + i, tmpPositions[i]);
				}
			}
		}
	}

}