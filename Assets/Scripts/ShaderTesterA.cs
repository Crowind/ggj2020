﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderTesterA : MonoBehaviour {

	private MeshRenderer rend;
	
	private void Start() {
		rend = GetComponent<MeshRenderer>();

		rend.material.SetFloat("_StartPointX", -3);
		rend.material.SetFloat("_StartPointY", 0);
		rend.material.SetFloat("_EndPointX", 3);
		rend.material.SetFloat("_EndPointY", 0);

		rend.material.SetFloat("_Duration", 5);
		float startTime = Time.time;
		rend.material.SetFloat("_StartTime", startTime);
	}

	// Update is called once per frame
	private void Update() {

		
		rend.material.SetFloat("_CurTime",Time.time);

	}
}