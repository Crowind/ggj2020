﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Crafting", menuName = "Scriptable/Crafting",order=2)]
public class EnumToSprite : ScriptableObject {

    [SerializeField]
    public List <SingleScrap> singleScraps;
    [SerializeField]
    public List<DoubleScrap> doubleScraps;

    [System.Serializable]
    public struct SingleScrap {

        public ScrapSettings.ScrapType type;
        public Sprite broken;
        public AudioClip audioClip;
    }
    
    [System.Serializable]
    public struct DoubleScrap {

        public ScrapSettings.ScrapType type1;
        public ScrapSettings.ScrapType type2;
        public Sprite repaired;
        public int score;

    }

}
