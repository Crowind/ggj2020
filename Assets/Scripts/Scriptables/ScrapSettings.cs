﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;


[CreateAssetMenu(fileName = "Data", menuName = "Scriptable/Scrap", order = 1)]
public class ScrapSettings : ScriptableObject
{
	public enum ScrapType
	{
		Banana, Umbrella, Phone, Slipper
	}

	public ScrapType scrapType = ScrapType.Banana;
	public static int scrapsSpawned=0;
	public AudioClip Sound;

	public Sprite BrokenSprite;
	public Sprite CompleteSprite;

	

}