﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour {


	public GameObject heartObj;

	private RectTransform containerT;
	private float health;

	private List<Image> sprites;

	public Sprite heart;
	public Sprite heartB;

	private void Start() {

		sprites = new List<Image>();

		containerT = GetComponent<RectTransform>();
		health = LevelManager.instance.health;

		for (int i = 0; i < health; i++) {

			GameObject obj = GameObject.Instantiate(heartObj, containerT);
			sprites.Add(obj.GetComponent<Image>());
		}


	}

	// Update is called once per frame
	private void Update() {
		health = LevelManager.instance.health;
		int i = 0;
		foreach (Image image in sprites) {
			if (i < health) {
				image.sprite = heart;
			}
			else {
				image.sprite = heartB;
			}
			i++;
		}
	}
}