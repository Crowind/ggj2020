using System;
using DesignPatterns;
using UnityEngine;

public class Falling : State<Scrap> {
	
	private Vector2 velocity;
	private Rigidbody2D rigidbody2D;
	
	public Falling(Scrap machine,Vector2 velocity) : base(machine)
	{
		rigidbody2D = machine.GetComponent<Rigidbody2D>();
		rigidbody2D.constraints = RigidbodyConstraints2D.None;
		rigidbody2D.velocity = velocity;
		
	}

	public override void HandleInput()
	{if (Input.GetMouseButton(0))
		{
			if (machine.CheckClick()) {
				machine.State = new Stopped(machine, rigidbody2D.velocity);
			}
		}
	}

	public override void Update()
	{
	}
}