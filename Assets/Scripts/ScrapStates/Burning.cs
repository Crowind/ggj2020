using DesignPatterns;

public class Burning : State<Scrap>
{

	public Burning(Scrap machine) : base(machine)
	{ }

	public override void HandleInput()
	{
		throw new System.NotImplementedException();
	}

	public override void Update()
	{
		throw new System.NotImplementedException();
	}
}