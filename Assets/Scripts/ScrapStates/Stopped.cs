using DesignPatterns;
using UnityEngine;

public class Stopped : State<Scrap> {
	
	private float maxUptime = 5;
	private float startTime;
	private Vector2 velocity; 
	
	public Stopped(Scrap machine, Vector2 velocity) : base(machine) {
		this.velocity = velocity;
		ConnectManager.instance.Add(machine);
		startTime = Time.time;
		machine.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
		
	}

	public override void HandleInput()
	{
		if (!Input.GetMouseButton(0)) {
			machine.State = new Falling(machine,velocity);
		}
	}

	public override void Update() {
		if (Time.time > startTime + maxUptime) {
			machine.State = new Falling(machine,velocity);
			ConnectManager.instance.ClearQueue();
		}
	}
}