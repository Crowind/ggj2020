﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ScrapSpawnerPool : MonoBehaviour
{
    public static ScrapSpawnerPool current;
    public GameObject pooledObject;
    public int poolAmount = 10;
    private ScrapSettings[] scrapArray;
    

    public float randomAreaHorizontalRangeMin;
    public float randomAreaHorizontalRangeMax;

    public float angleHorizontalMin;
    public float angleHorizontalMax;
    public float angleVerticalMin;
    public float angleVerticalMax;
    public float interval;
    private float startTime;

    private bool isFirstLaunch = true;
    
    public float spawnModifier = 1;

    public float randomRotation;
    
    public float force;
    public int index = 0;


    List<GameObject> pooledScraps;

    // Start is called before the first frame update
    void Start() {
        startTime = Time.time;
        scrapArray = new ScrapSettings[Resources.LoadAll<ScrapSettings>("").Length];
        pooledScraps = new List<GameObject>();

        for (int i = 0; i < poolAmount; i++) {
            GameObject obj = (GameObject)Instantiate(pooledObject);
            obj.SetActive(false);
            pooledScraps.Add(obj);
        }

        StartCoroutine(SpawnRoutine());
    }


    private GameObject SpawnScrap() {
        if (index == poolAmount) { index = 0; }
        if (!pooledScraps[index].activeInHierarchy) {
                index++;
                return pooledScraps[index - 1]; 
        }

        return null;
    }

    public void IncreaseDifficulty()
    {
        spawnModifier -= 0.024f;
    }
    
    IEnumerator SpawnRoutine()
    {
        while (true)
        {
            if (isFirstLaunch)
            {
                for (int i = 0; i < 2; i++)
                {
                    var spawnedObject = SpawnScrap();
                    if (spawnedObject)
                    {






                        var sType = (ScrapSettings.ScrapType)Random.Range(0, (int)Enum.GetValues(typeof(ScrapSettings.ScrapType)).Cast<ScrapSettings.ScrapType>().Max() + 1);

                        spawnedObject?.SetActive(true);

                        EnumToSprite[] enumToSprite = Resources.LoadAll<EnumToSprite>("");

                        spawnedObject.GetComponent<SpriteRenderer>().sprite = ScriptableUtility.Get(sType, enumToSprite[0].singleScraps).Item1;
                        Destroy(spawnedObject.GetComponent<Collider2D>());
                        spawnedObject.AddComponent<PolygonCollider2D>();
                        spawnedObject.GetComponent<Rigidbody2D>().angularVelocity += Random.Range(-randomRotation, randomRotation);
                        spawnedObject.transform.position = RandomArea();
                        spawnedObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                        var scrap = spawnedObject.GetComponent<Scrap>();
                        scrap.AddForce(RandomAngle(), force);
                        scrap.type = sType;

                        FindObjectOfType<SoundManager>().PlayLaunchSound();
                    }
                    yield return new WaitForSeconds(0.5f * spawnModifier);
                }
                isFirstLaunch = false;
            }
            else
            {

                for (int i = 0; i < 3; i++)
                {


                    var spawnedObject = SpawnScrap();
                    if (spawnedObject)
                    {






                        var sType = (ScrapSettings.ScrapType)Random.Range(0, (int)Enum.GetValues(typeof(ScrapSettings.ScrapType)).Cast<ScrapSettings.ScrapType>().Max() + 1);

                        spawnedObject?.SetActive(true);

                        EnumToSprite[] enumToSprite = Resources.LoadAll<EnumToSprite>("");
                       
                        spawnedObject.GetComponent<SpriteRenderer>().sprite =  ScriptableUtility.Get(sType, enumToSprite[0].singleScraps).Item1;
                        Destroy(spawnedObject.GetComponent<Collider2D>());
                        spawnedObject.AddComponent<PolygonCollider2D>();
                        spawnedObject.GetComponent<Rigidbody2D>().angularVelocity += Random.Range(-randomRotation, randomRotation);
                        spawnedObject.transform.position = RandomArea();
                        spawnedObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                        var scrap = spawnedObject.GetComponent<Scrap>();
                        scrap.AddForce(RandomAngle(), force);
                        scrap.type = sType;

                        FindObjectOfType<SoundManager>().PlayLaunchSound();
                    }
                    yield return new WaitForSeconds(0.5f * spawnModifier);
                }
            }

            yield return new WaitForSeconds(3*spawnModifier);
        }
    }
    
    private Vector2 RandomArea()
    {
        float horizontalPoint = Random.Range(randomAreaHorizontalRangeMin, randomAreaHorizontalRangeMax);
        
        return new Vector2(horizontalPoint, transform.position.y);
    }

    private Vector2 RandomAngle()
    {
        float horizontalAngle = Random.Range(angleHorizontalMin, angleHorizontalMax);
        float verticalAngle = Random.Range(angleVerticalMin, angleVerticalMax);
        
        return new Vector2(horizontalAngle, verticalAngle);
    }

    private ScrapSettings.ScrapType RandomScrap() {

        float rand = Random.Range(0f, 1f);

        if (rand < 0.15f) {
            return ScrapSettings.ScrapType.Banana;
        }
        if (rand < 0.35f) {
            return ScrapSettings.ScrapType.Umbrella;
        }
        return rand < 0.65f ? ScrapSettings.ScrapType.Phone : ScrapSettings.ScrapType.Umbrella;

    }

}

