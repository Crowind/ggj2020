﻿Shader "Custom/NoiseDistortionHighlight"
{
     Properties
     {
        _DistTex ("Distortion", 2D) = "white" {}
        _Amount ("Amount", Range(0,1)) = 0.5
        _Color("Color",Color) = (0,0,0,0)
//        _StartPointX("Start Point X",float) = 0
//        _StartPointY("Start Point Y",float) = 0
//        _EndPointX("End Point X",float) = 0
//        _EndPointY("End Point Y",float) = 0
        _Duration("Duration",float) = 5
        _Speed("Speed",range(0.1,5)) = 1
        _Exp("Exponent",range(1,10)) = 1
        
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }


        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            
            float _Amount;
            fixed4 _Color;
            sampler2D _DistTex;
            float4 _DistTex_ST;
            float _Speed;
            
            
            float _StartPointX;
            float _StartPointY;
            float2 _StartPoint;
            float _EndPointX;
            float _EndPointY;
            float2 _EndPoint;
            float2 _MidPoint;
            float _Duration;
            float _StartTime;
            float _CurTime;
            float _Exp;            
            

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float proximity : BLENDWEIGHT0;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.proximity = 0;
                float delta = sin(v.vertex.x - v.vertex.y);
                o.vertex.x += (tex2Dlod(_DistTex, _DistTex_ST *  sin(_Time) + delta  ).r * 2 - 1)  * _Amount ;
                o.vertex.y += (tex2Dlod(_DistTex, _DistTex_ST *  sin(_Time) + delta  ).g * 2 - 1)  * _Amount ;         
                
                
                float2 worldVertex =  (mul (unity_ObjectToWorld , o.vertex)).xy;
                _MidPoint.x = (_StartPointX + _EndPointX)/2;
                _MidPoint.y = (_StartPointY + _EndPointY)/2;
                _StartPoint.x = _StartPointX;
                _StartPoint.y = _StartPointY;
                _EndPoint.x = _EndPointX;
                _EndPoint.y = _EndPointY;
                
                
                float cutoffDistance = lerp( 0 , distance(_StartPoint, _EndPoint) /2 ,  min( _Speed *(pow(_CurTime,_Exp) - _StartTime)/ (_Duration ) , 1 ) );   
                 
                if( distance(worldVertex,_MidPoint) < cutoffDistance ){
                
                    o.proximity = 1;
                }
                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target {
                return  ( (i.proximity <= 0) ? _Color.rgba : fixed4(1,1,0,1) ) ;
            }
            ENDCG
        }
    }
}
