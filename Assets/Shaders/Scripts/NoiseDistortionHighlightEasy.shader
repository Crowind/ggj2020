﻿Shader "Custom/NoiseDistortionHighlightEasy"
{
     Properties
     {
        _MoveTex ("Moving Texture",2D) = "white" {}
        _DistTex ("Distortion", 2D) = "white" {}
        _Amount ("Amount", Range(0,1)) = 0.5
        _Highlight("Highlight Color",Color) = (0,0,0,0)
        _Speed("Speed",range(0.1,5)) = 1
        _TexAmount("Texture Amount",range(0,1)) = 0.5
        
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }


        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            
            float _Amount;
            fixed4 _Highlight;
            sampler2D _MoveTex;
            float4 _MoveTex_ST;
            sampler2D _DistTex;
            float4 _DistTex_ST;
            float _Speed;
            
            float _TexAmount;
            float _StartPointX;
            float _StartPointY;
            float2 _StartPoint;
            float _EndPointX;
            float _EndPointY;
            float2 _EndPoint;
            float2 _MidPoint;
            float _Duration;
            float _StartTime;
            float _CurTime;
            float _Exp;            
            

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MoveTex);
                float delta = sin(v.vertex.x - v.vertex.y);
                o.vertex.x += (tex2Dlod(_DistTex, _DistTex_ST *  sin(_Time) + delta  ).r * 2 - 1)  * _Amount ;
                o.vertex.y += (tex2Dlod(_DistTex, _DistTex_ST *  sin(_Time) + delta  ).g * 2 - 1)  * _Amount ;         
                return o;
            }

            fixed4 frag (v2f i) : SV_Target {
            
                fixed4 col = _TexAmount * tex2D(_MoveTex, i.uv + _Time * -_Speed) + (1-_TexAmount) * _Highlight;

                return  col;
            }
            ENDCG
        }
    }
}
