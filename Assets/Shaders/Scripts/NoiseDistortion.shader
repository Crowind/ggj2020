﻿Shader "Custom/NoiseDistortion"
{
     Properties
     {
        _DistTex ("Distortion", 2D) = "white" {}
        _Amount ("Amount", Range(0,1)) = 0.5
        _Color("Color",Color) = (0,0,0,0)
        
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }


        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            
            float _Amount;
            fixed4 _Color;
            sampler2D _DistTex;
            float4 _DistTex_ST;
            

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                float delta = sin(v.vertex.x - v.vertex.y);
                o.vertex.x += (tex2Dlod(_DistTex, _DistTex_ST *  sin(_Time) + delta  ).r * 2 - 1)  * _Amount ;
                o.vertex.y += (tex2Dlod(_DistTex, _DistTex_ST *  sin(_Time) + delta  ).g * 2 - 1)  * _Amount ;                
                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target {

                
                return _Color.rgba;
            }
            ENDCG
        }
    }
}
